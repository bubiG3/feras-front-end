import { Node } from './map/node';
import { SimVariableService } from './sisVariables/SimVariables.service';
import { Brush } from './map/brush';
import { Image } from './map/image';
import { Injectable, OnInit, EventEmitter } from '@angular/core';
import { Map } from './map/map';
import { RequestService } from "./request/request.service";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapService{

  public isColored: boolean = false;

  private currentBrush: string = null;
  public image: Image = null;
  public currentMap: Map;
  private tempFirstCoordinates = {
    x: null,
    y: null
  };

  private selectedEndCell = {
    x: null,
    y: null
  };

  public nodeAdded: EventEmitter<boolean>;

  private apiData = new BehaviorSubject<any>(null);
  public apiData$ = this.apiData.asObservable();

  constructor(private mapServ: RequestService, private simVarService: SimVariableService) {
    this.nodeAdded = new EventEmitter();
  }

  changeBrush(brush:Brush){
    if(brush == Brush.Eraser){
      this.currentBrush = null;
    }
    else{
      this.currentBrush = Brush[brush];
    }
  }

  getNode(x:number,y:number):Node{
      return this.currentMap.getNode(x,y);
  }

  setImageData(data: any) {
    this.image = new Image(data);
  }

  setGridSize(imageWidth: number, imageHeight: number) {
    this.image.setGridSize(imageWidth, imageHeight);
    this.currentMap = new Map(this.image.getCellsHorizontal(), this.image.getCellsVertical());
  }
  
  getGridSize() {
    return this.image.getGridSize();
  }

  increaseCellSize() {
    this.image.increaseCellSize();
    this.currentMap = new Map(this.image.getCellsHorizontal(),this.image.getCellsVertical());
    setTimeout(() => {
      this.nodeAdded.emit(true);
    }, 4000/6);

    this.isColored = false;
  }

  decreaseCellSize() {
    this.image.decreaseCellSize();
    this.currentMap = new Map(this.image.getCellsHorizontal(),this.image.getCellsVertical());
    this.nodeAdded.emit(true);
    this.isColored = false;
  }

  OnMouseDown(x: number, y:number){
    this.tempFirstCoordinates.x = x;
    this.tempFirstCoordinates.y = y;
  }

  OnMouseUp(x: number, y:number){
    this.selectedEndCell.x = null;
    this.selectedEndCell.y = null;

    if(this.tempFirstCoordinates.x != null && this.tempFirstCoordinates.y != null){
      const firstCoordinates = {x:null,y:null}
      const secondCoordinates = {x:null,y:null}
      firstCoordinates.x = this.tempFirstCoordinates.x >= x ? x : this.tempFirstCoordinates.x;
      secondCoordinates.x = this.tempFirstCoordinates.x >= x ? this.tempFirstCoordinates.x : x;
      // if(this.tempFirstCoordinates.x >= x){
      //   firstCoordinates.x = x;
      //   secondCoordinates.x = this.tempFirstCoordinates.x;
      // }
      // else{
      //   firstCoordinates.x = this.tempFirstCoordinates.x;
      //   secondCoordinates.x = x;
      // }
      firstCoordinates.y = this.tempFirstCoordinates.y >= y ? y : this.tempFirstCoordinates.y;
      secondCoordinates.y = this.tempFirstCoordinates.y >= y ? this.tempFirstCoordinates.y : y;
      // if(this.tempFirstCoordinates.y >= y){
      //   firstCoordinates.y = y;
      //   secondCoordinates.y = this.tempFirstCoordinates.y;
      // }
      // else{
      //   firstCoordinates.y = this.tempFirstCoordinates.y;
      //   secondCoordinates.y = y;
      // }
      for(var i = firstCoordinates.x; i<= secondCoordinates.x; i++){
        for(var z = firstCoordinates.y; z <= secondCoordinates.y; z++){
            this.currentMap.getNode(i,z).setMapObject(this.currentBrush);
            this.nodeAdded.emit(true);
        }
      }
    }
    else{
      console.log("An unexpected error occured! No first Node selected!");
    }
    this.tempFirstCoordinates.x = null;
    this.tempFirstCoordinates.y = null;
    this.isColored = true;
  }

  OnMouseOver(x:number,y:number){
    if(this.tempFirstCoordinates.x != null){
      this.selectedEndCell.x = x;
      this.selectedEndCell.y = y;
      this.nodeAdded.emit(false);
    }
  }

  CheckIfSelected(x:number,y:number):boolean{
    if(this.tempFirstCoordinates.x != null && this.selectedEndCell.x != null){
      const firstCoordinates = {x:null,y:null}
      const secondCoordinates = {x:null,y:null}

      if(this.tempFirstCoordinates.x >= this.selectedEndCell.x){
        firstCoordinates.x = this.selectedEndCell.x;
        secondCoordinates.x = this.tempFirstCoordinates.x;
      }
      else{
        firstCoordinates.x = this.tempFirstCoordinates.x;
        secondCoordinates.x = this.selectedEndCell.x;
      }
      if(this.tempFirstCoordinates.y >= this.selectedEndCell.y){
        firstCoordinates.y = this.selectedEndCell.y;
        secondCoordinates.y = this.tempFirstCoordinates.y;
      }
      else{
        firstCoordinates.y = this.tempFirstCoordinates.y;
        secondCoordinates.y = this.selectedEndCell.y;
      }

      if(firstCoordinates.x <= x && secondCoordinates.x >= x && firstCoordinates.y <= y && secondCoordinates.y >= y){
        return true;
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }

  }

  public getMap() {
    return this.currentMap.getMap();
  }

  public RunTest() {
    const variablesVM = this.simVarService.getData();

    return this.mapServ.post(
      'Simulation/RunSimulation',
      { nodes: this.currentMap.array, variablesVM }
    )
      .subscribe(
        (data: any) => {
          this.setData(data);
        },
        (error: any) => {
          console.log(error);
        }
      );
  }
  setData(data) {
    this.apiData.next(data);
  }

}
