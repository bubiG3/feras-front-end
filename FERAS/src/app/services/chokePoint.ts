export class ChokePoint{
    x: number;
    y: number;
    numberOfIntersections: number;
}