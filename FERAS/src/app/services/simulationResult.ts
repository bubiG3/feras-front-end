import { ChokePoint } from './chokePoint';

export class SimulationResult{
    data: {
        result: {
            averageTime: number,
            longestTime: number,
            shortestTime: number,
            chokePoints: ChokePoint[]
        },
        numberOfSimulations: number,
        numberOfDeadPeople: number
    }
}