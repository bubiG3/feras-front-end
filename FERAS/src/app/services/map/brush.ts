export enum Brush {
  Wall,
  Floor,
  Door,
  Exit,
  Eraser
}
