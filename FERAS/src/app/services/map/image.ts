export class Image{

  public imageBase64;
  private width: number = null;
  private height: number = null;
  private size;

  private cellSize: number = null;
  private cellScale: number = 5;

  private cellsHorizontal: number = null;
  private cellsVertical: number = null;

  constructor(imageFile: any){
    var reader = new FileReader();
    reader.readAsDataURL(imageFile);
    reader.onload = (event) => {
      this.imageBase64 = event.target.result;
    }
  }

  setGridSize(width: number, height: number){
    this.width = width;
    this.height = height;
    this.setParameters();
  }

  getGridSize(){
    this.size = [this.width, this.height];
    return this.size;
  }

  private setParameters(){
    this.cellSize = this.width * this.cellScale / 100;
    this.cellsHorizontal = Math.round((this.width / this.cellSize) + 1);
    this.cellsVertical = Math.round((this.height / this.cellSize) + 1);
  }

  increaseCellSize(){
    if(this.cellScale <= 5){
      this.cellScale += .3;
      this.setParameters();
    }
  }

  decreaseCellSize(){
    if(this.cellScale >= .5){
      this.cellScale -= .3;
      this.setParameters()
    }
  }

  getCellsHorizontal(){
    return this.cellsHorizontal;
  }

  getCellsVertical(){
    return this.cellsVertical;
  }

}
