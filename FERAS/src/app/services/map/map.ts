import { Node } from './node';

export class Map {
  //Probably needed for future resizing?
  private mapWidth: number;
  private mapHeight: number;

  public array: Node[][];

  constructor(mapWidth: number, mapHeight: number) {
    this.mapWidth = mapWidth;
    this.mapHeight = mapHeight;
    this.array = new Array(this.mapHeight);
    for (let i = 0; i < this.mapHeight; i++) {
      this.array[i] = new Array(this.mapWidth);
    }

    for(let x = 0; x < this.mapWidth; x++){
      for(let y = 0; y < this.mapHeight; y++){
        this.createNode(x,y);
      }
    }
  }

  public getMap() {
    return this.array;
  }

  private createNode(x: number, y: number): void {
    this.array[y][x] = new Node(x,y);
  }

  public getNode(x: number, y:number): Node {
    return this.array[y][x];
  }
}
