export interface IMapObject{

}

export enum MapObject {
    Door = "Door",
    Exit = "Exit",
    Fire = "Fire",
    Floor = "Floor",
    Person = "Person",
    Wall = "Wall",
}