export class Node {

    public x: number;
    public y: number;
    public mapObject: string;

    public constructor(x: number, y: number){
        this.x = x;
        this.y = y;
        this.mapObject = null;
    }

    public setMapObject(mapObject: string){
      this.mapObject = mapObject;
    }
}
