import { PeopleSpeed } from './PeopleSpeed';

export class SimVariablesVM {


  constructor(
    public nrRuns: number,
    public nrPeople: number,
    public sizeCells: number,
    public speed: PeopleSpeed,
    public fireSpreadSpeed: number) {
  }
}
