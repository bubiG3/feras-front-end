import { SimVariablesVM } from './SimVariableVM';
import { Injectable } from '@angular/core';
import { PeopleSpeed } from './PeopleSpeed';


@Injectable({
  providedIn: 'root'
})

export class SimVariableService {

  private currentSettings: SimVariablesVM = new SimVariablesVM(10, 5, 50, new PeopleSpeed(1.7, 3.1), 0.1);

  public getData(): SimVariablesVM {
    return this.currentSettings;
  }

  public setData(data: SimVariablesVM) {
    this.currentSettings = data;
  }
}
