import { MatDialogModule } from '@angular/material/dialog';
// * General Modules
import { AppRoutingModule } from './app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

// * Icons
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';

// * Angular material
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { HttpClientModule } from "@angular/common/http";
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSliderModule } from '@angular/material/slider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { Ng5SliderModule } from 'ng5-slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';

// * Components
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { ToolBarComponent } from './components/tool-bar/tool-bar.component';
import { AppComponent } from './app.component';
import { ResultsComponent } from './components/results/results.component';
import { DisplayMapComponent } from './components/display-map/display-map.component';
import { SettingsComponent } from './components/settings/settings.component';
import { MapCellComponent } from './components/display-map/map-cell/map-cell.component';
import { ConfirmDialogComponent } from './components/home/confirm-dialog/confirm-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    ToolBarComponent,
    ResultsComponent,
    DisplayMapComponent,
    SettingsComponent,
    MapCellComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    ReactiveFormsModule,

    // * Angular material
    MatButtonModule,
    MatDividerModule,
    FontAwesomeModule,
    MatCardModule,
    HttpClientModule,
    DragDropModule,
    MatSliderModule,
    MatTooltipModule,
    Ng5SliderModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far);
  }
}

