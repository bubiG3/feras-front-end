import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { Component, OnInit, HostListener } from '@angular/core';
import { MapService } from '../../services/map.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('openClose', [
      state('true', style({
        top: "0px"
      })),
      state('false', style({
        top: "-90px"
      })),
      transition('true <=> false', [
        animate('0.5s')
      ]),
    ]),
    trigger('showHide', [
      state('true', style({
        height: "100vh",
        'margin-top': 0
      })),
      state('false', style({
        height: "calc(100vh - 90px)",
        'margin-top': "90px"
      })),
      transition('true <=> false', [
        animate('0.5s')
      ]),
    ]),
  ],
})
export class HomeComponent implements OnInit {

  isOpen: boolean = true;
  showHide : boolean = false;
  interval = null;

  startingWidth: number = 0;
  startingHeight: number = 0;
  scale = 1;

  constructor(private mapService: MapService, public dialog: MatDialog) { }

  ngOnInit(): void {
    // provides some testing results
    // console.log(this.mapService.hardCodedMap.getMap());
  }

  FullScreen(){
      document.documentElement.requestFullscreen();
      this.isOpen = false;
      this.showHide = true;
  }

  OpenClose(){
    this.isOpen = !this.isOpen;
    this.showHide = !this.showHide;
  }

  MoveScreen(direction: string){
    // Elements
    var map = document.getElementById("map");
    var screen = document.getElementById("screen");

    // Needed for tracking the movement
    var horizontalPosition = map.getBoundingClientRect().left;
    var verticalPosition = map.getBoundingClientRect().top;

    // Stops on reaching max right and bottom
    var maxRight = screen.getBoundingClientRect().width - map.getBoundingClientRect().width;
    var maxBottom = screen.getBoundingClientRect().height - map.getBoundingClientRect().height;

    // if(map.getBoundingClientRect().width > screen.getBoundingClientRect().width ||
    //   map.getBoundingClientRect().height > screen.getBoundingClientRect().height){
        if(direction == "right"){
          this.interval = setInterval(function(){
            if(map.getBoundingClientRect().left > maxRight - 200) {
              map.style.left = horizontalPosition + "px";
              horizontalPosition -= 10;
            }
          }, 10);
        }
        else if (direction == "left"){
          this.interval = setInterval(function(){
            if(map.getBoundingClientRect().left < screen.getBoundingClientRect().left + 200) {
              map.style.left = horizontalPosition + "px";
              horizontalPosition += 10;
            }
          }, 10);
        }
        else if (direction == "bottom"){
          this.interval = setInterval(function(){
            if(map.getBoundingClientRect().top > maxBottom - 200) {
              map.style.top = verticalPosition + "px";
              verticalPosition -= 10;
            }
          }, 10);
        }
        else if (direction == "top"){
          this.interval = setInterval(function(){
            if(map.getBoundingClientRect().top < screen.getBoundingClientRect().top + 200) {
              map.style.top = verticalPosition + "px";
              verticalPosition += 10;
            }
          }, 10);
        }


        // Diagonals
        if (direction == "bottom-right"){
          this.interval = setInterval(function(){
            if(map.getBoundingClientRect().left > maxRight - 200) {
              map.style.left = horizontalPosition + "px";
              horizontalPosition -= 8;
            }
            if(map.getBoundingClientRect().top > maxBottom - 200) {
              map.style.top = verticalPosition + "px";
              verticalPosition -= 8;
            }
          }, 10);
        }
        else if (direction == "bottom-left"){
          this.interval = setInterval(function(){
            if(map.getBoundingClientRect().left < screen.getBoundingClientRect().left + 200) {
              map.style.left = horizontalPosition + "px";
              horizontalPosition += 8;
            }
            if(map.getBoundingClientRect().top > maxBottom - 200) {
              map.style.top = verticalPosition + "px";
              verticalPosition -= 8;
            }
          }, 10);
        }
        else if (direction == "top-left"){
          this.interval = setInterval(function(){
            if(map.getBoundingClientRect().left < screen.getBoundingClientRect().left + 200) {
              map.style.left = horizontalPosition + "px";
              horizontalPosition += 8;
            }
            if(map.getBoundingClientRect().top < screen.getBoundingClientRect().top + 200) {
              map.style.top = verticalPosition + "px";
              verticalPosition += 8;
            }
          }, 10);
        }
        else if (direction == "top-right"){
          this.interval = setInterval(function(){
            if(map.getBoundingClientRect().left > maxRight - 200) {
              map.style.left = horizontalPosition + "px";
              horizontalPosition -= 8;
            }
            if(map.getBoundingClientRect().top < screen.getBoundingClientRect().top + 200) {
              map.style.top = verticalPosition + "px";
              verticalPosition += 8;
            }
          }, 10);
        }
    // }
  }

  increaseCellSize(){
    if(this.mapService.isColored){
      const dialogRef = this.dialog.open(ConfirmDialogComponent);
      dialogRef.componentInstance.indicateIncrease = true;
      dialogRef.componentInstance.indicateDecrease = false;
    }
    else{
      this.mapService.increaseCellSize();
    }
  }

  decreaseCellSize(){
    if(this.mapService.isColored){
      const dialogRef = this.dialog.open(ConfirmDialogComponent);
      dialogRef.componentInstance.indicateDecrease = true;
      dialogRef.componentInstance.indicateIncrease = false;
    }
    else{
      this.mapService.decreaseCellSize();
    }
  }

  getImage(){
    return this.mapService.image;
  }

  getMap(){
    return this.mapService.currentMap;
  }

  Stop(){
    clearInterval(this.interval);
  }

  @HostListener('mousewheel', ['$event']) onMousewheel(event) {
    if(this.mapService.image){
      var map = document.getElementById("map");
      this.startingWidth = this.mapService.getGridSize()[0];
      this.startingHeight = this.mapService.getGridSize()[1];

      if(event.wheelDelta>0){
        if(this.scale < 6){
          this.scale += .3;
        }
      }
      if(event.wheelDelta<0){
        if(this.scale > -3){
          this.scale -= .3;
        }
      }
      map.style.width = this.startingWidth * this.scale + "px";
      map.style.height = this.startingHeight * this.scale + "px";
    }
  }
}
