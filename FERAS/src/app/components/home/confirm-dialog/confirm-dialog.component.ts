import { MapService } from './../../../services/map.service';
import { HomeComponent } from './../home.component';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  indicateIncrease: boolean;
  indicateDecrease: boolean;

  constructor(private mapService: MapService,public dialogRef: MatDialogRef<ConfirmDialogComponent>) { }

  ngOnInit(): void {
  }

  changeSize(){
    if(this.indicateIncrease){
      this.mapService.increaseCellSize();
    }
    if(this.indicateDecrease){
      this.mapService.decreaseCellSize();
    }
    this.dialogRef.close();

  }

  cancel(){
    this.dialogRef.close();
  }

}
