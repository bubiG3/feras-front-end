import { Component, OnInit } from '@angular/core';
import { MapService } from '../../services/map.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  currentFile = null;

  constructor(private mapService: MapService) { }

  ngOnInit(): void {

  }

  // This is the function triggered when the RUN button is clicked
  public Run(){
    this.mapService.RunTest();
  }

  onFileSelect(event){
    if(event.target.files && event.target.files[0]){
      const file = event.target.files[0];
      this.mapService.setImageData(file);
    }
  }

}
