import { Component, OnInit, OnDestroy } from '@angular/core';
import { SimulationResult } from '../../services/simulationResult';
import { MapService } from 'src/app/services/map.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit, OnDestroy {

  simulationResult: SimulationResult;
  subscription: Subscription;

  constructor(private mapService: MapService) { 
  }

  ngOnInit() {
    this.subscription = this.mapService.apiData$.subscribe((data: SimulationResult) => {
      this.simulationResult = data;
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
