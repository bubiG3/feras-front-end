import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayMapPartialComponent } from './display-map-partial.component';

describe('DisplayMapPartialComponent', () => {
  let component: DisplayMapPartialComponent;
  let fixture: ComponentFixture<DisplayMapPartialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayMapPartialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayMapPartialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
