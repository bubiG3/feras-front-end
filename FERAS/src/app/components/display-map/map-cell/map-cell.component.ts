import { Node } from './../../../services/map/node';
import { MapService } from 'src/app/services/map.service';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ChokePoint } from 'src/app/services/chokePoint';


@Component({
  selector: 'app-map-cell',
  templateUrl: './map-cell.component.html',
  styleUrls: ['./map-cell.component.scss']
})
export class MapCellComponent implements OnInit {

  @Input() x: number;
  @Input() y: number;

  @Input() chokePointIndex: number = 0;

  currentNode: Node;
  isSelected: string = null;

  eventListener: Subscription;

  constructor(private mapService: MapService, private router: Router) {
    this.eventListener = mapService.nodeAdded.subscribe(isDrawn => this.changeNode(isDrawn));
  }

  ngOnInit(): void {
    this.currentNode = this.mapService.getNode(this.x, this.y);
  }

  ngOnDestroy(): void {
    this.eventListener.unsubscribe();
  }

  changeNode(isDrawn: boolean) {
    if (isDrawn) {
      this.isSelected = null;
      this.currentNode = this.mapService.getNode(this.x, this.y);
    }
    else {
      if (this.mapService.CheckIfSelected(this.currentNode.x, this.currentNode.y)) {
        this.isSelected = "selected";
      }
      else {
        this.isSelected = null;
      }
    }
  }

  OnMouseEvent(event: string) {
    if (this.router.url != "/results")
      switch (event) {
        case "over":
          this.mapService.OnMouseOver(this.currentNode.x, this.currentNode.y);
          break;
        case "up":
          this.mapService.OnMouseUp(this.currentNode.x, this.currentNode.y);
          break;
        case "down":
          this.mapService.OnMouseDown(this.currentNode.x, this.currentNode.y);
          break;
      }
  }
  GetClass(): string {
    if (this.chokePointIndex > 0) {
      if (this.chokePointIndex <= 1) {
        return "choke-5";
      }
      else if (this.chokePointIndex <= 2) {
        return "choke-10";
      }
      else if (this.chokePointIndex <= 3) {
        return "choke-15";
      }
      else {
        return "choke-20";
      }
    }
  }
}