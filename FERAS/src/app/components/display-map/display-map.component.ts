import { MapService } from '../../services/map.service';
import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import { ChokePoint } from 'src/app/services/chokePoint';

@Component({
  selector: 'app-display-map',
  templateUrl: './display-map.component.html',
  styleUrls: ['./display-map.component.scss']
})
export class DisplayMapComponent implements OnInit {

  @Input() chokePoints: ChokePoint[] = [];

  @ViewChild('container', {read: ElementRef, static:false}) elementView: ElementRef;

  counter(i: number) {
    return new Array(i);
  }

  constructor(private mapService: MapService) { }

  ngOnInit() {
    
  }

  setImage(){
    let imageWidth = this.elementView.nativeElement.offsetWidth;
    let imageHeight = this.elementView.nativeElement.offsetHeight;
    this.mapService.setGridSize(imageWidth, imageHeight);
  }

  getImage(){
    return this.mapService.image;
  }

  getMap(){
    return this.mapService.currentMap;
  }

  getNode(x: number, y: number){
    return this.mapService.getNode(x, y);
  }

  SendIntersections(x: number, y: number): number{
    var i;
    var node = this.getNode(x, y);
    for (i = 0; i < this.chokePoints.length; i++) {
      if (this.chokePoints[i].x == node.x && this.chokePoints[i].y == node.y) {
        return this.chokePoints[i].numberOfIntersections;
      }
    }
    return 0;
  }
}
