import { MapService } from './../../services/map.service';
import { Brush } from './../../services/map/brush';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.scss']
})
export class ToolBarComponent implements OnInit {

  currentBrush = Brush;

  constructor(private mService: MapService) { }

  ngOnInit(): void {
  }

  changeBrush(brush: Brush){
    this.mService.changeBrush(brush);
  }

}
