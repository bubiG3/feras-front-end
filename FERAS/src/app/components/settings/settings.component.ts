import { SimVariableService } from '../../services/sisVariables/SimVariables.service';
import { PeopleSpeed } from '../../services/sisVariables/PeopleSpeed';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SimVariablesVM } from 'src/app/services/sisVariables/SimVariableVM';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent implements OnInit {

  //Default values
  defaultNumberOfRuns = 10;
  defaultNumberOfPeople = 5;
  defaultPeopleMS = [1.7, 3.1];
  defaultCellSize = 50;
  defaultFireSpreadSpeed = 0.1;

  // Current values
  numberOfRuns = 10;
  numberOfPeople = 5;
  peopleMS = [1.7, 3.1];
  cellSize = 50;
  fireSpreadSpeed = 0.1;

  // Defining the form
  variablesForm: FormGroup;

  constructor(
    private variableService: SimVariableService,
    private router: Router,
    private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    var simValues: SimVariablesVM = this.variableService.getData();

    this.variablesForm = new FormGroup({
      'numberOfRuns': new FormControl(simValues.nrRuns),
      'numberOfPeople': new FormControl(simValues.nrPeople),
      'peopleMS': new FormControl([simValues.speed.minSpeed, simValues.speed.maxSpeed]),
      'cellSize': new FormControl(simValues.sizeCells),
      'fireSpreadSpeed': new FormControl(simValues.fireSpreadSpeed)
    });
  }

  onSubmit() {
    this.variableService.setData(
      new SimVariablesVM(
        this.numberOfRuns,
        this.numberOfPeople,
        this.cellSize,
        new PeopleSpeed(this.peopleMS[0], this.peopleMS[1]),
        this.fireSpreadSpeed));

    this.router.navigateByUrl("/");
    this.snackbar.open("Settings changed!", "close", {
      duration: 2000,
    });
  }

  restore() {
    this.variablesForm.reset({
      'numberOfRuns': this.defaultNumberOfRuns,
      'numberOfPeople': this.defaultNumberOfPeople,
      'peopleMS': this.defaultPeopleMS,
      'cellSize': this.defaultCellSize,
      'fireSpreadSpeed': this.defaultFireSpreadSpeed
    });
  }
}
